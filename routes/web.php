<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

// Auth - ból csak a Login - t meghagytam, regisztrálás nem kell (1 default User lesz)
Route::get('/login', 'Auth\LoginController@showLoginForm')->name('login');
Route::post('/login', 'Auth\LoginController@login');
Route::post('/logout', 'Auth\LoginController@logout')->name('logout');

Route::get('/categories','CategoriesController@show')->name('categories.show');
Route::post('/categories/create','CategoriesController@create')->name('categories.create');
Route::post('/categories/download/','DocumentsController@download')->name('documents.download');
Route::post('/categories/document/create','DocumentsController@create')->name('documents.create');
Route::post('/categories/permissions/edit','PermissionsController@edit')->name('permissions.edit'); // bár ez inkább User permission, URI lehetne épp más is


<ul>
    @foreach($childs as $child)
        <li>
            <form action="" method="GET">
                @csrf
                <button>{{ $child->name }}</button>

                <input name="selected_category_id" type="hidden" value="{{ $child->id }}" />
                <input name="name" type="hidden" value="{{ $child->name }}" />
            </form>
            
            

            @if(count($child->childs))
                @include('categories.manageChild',['childs' => $child->childs])
            @endif
        </li>
    @endforeach
</ul>
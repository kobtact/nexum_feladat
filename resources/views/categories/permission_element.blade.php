<div class="col-md-6">
    <form role="form" id="document" method="POST" action="{{ route('permissions.edit') }}" enctype="multipart/form-data">
        @csrf

        <input type="hidden" id="id" name="id" value="{{ $permission->id }}"/>
        <input type="hidden" id="user_id" name="user_id" value="{{ $user_id }}" />

        <div class="form-group {{ $errors->has('category_id') ? 'has-error' : '' }}">

            <label>{{ $category_name }}:</label>
            <select id="category_id" name="category_id" class="form-control">
                    <option value="{{ $permission->category_id }}">{{ $category_name }}</option>
            </select>

            @if ($errors->has('category_id'))
                <span class="text-red" role="alert">
                    <strong>{{ $errors->first('category_id') }}</strong>
                </span>
            @endif

        </div>

        <div class="form-check {{ $errors->has('upload') ? 'has-error' : '' }}">

            <label>Upload:</label>

        <input type="checkbox" id="upload" name="upload" class="form-check-input" {{ $permission->upload ? "checked" : "" }} />
            @if ($errors->has('upload'))
                <span class="text-red" role="alert">
                    <strong>{{ $errors->first('upload') }}</strong>
                </span>
            @endif

        </div>

        <div class="form-check {{ $errors->has('download') ? 'has-error' : '' }}">

            <label>Download:</label>

            <input type="checkbox" id="download" name="download" class="form-check-input" {{ $permission->download ? "checked" : "" }} />
            @if ($errors->has('download'))
                <span class="text-red" role="alert">
                    <strong>{{ $errors->first('download') }}</strong>
                </span>
            @endif

        </div>

        <div class="form-group">
            <button type="submit" class="btn btn-success">Modify permission</button>
        </div>

        </form>
</div>
@extends('layouts.app')

@section('content')
<!DOCTYPE html>

<html>

<head>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.6.3/css/font-awesome.min.css" />
    <link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/css/bootstrap.min.css" rel="stylesheet">
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>

    <link href="{{ asset('css/treeview.css') }}" rel="stylesheet">

</head>

<body>

<div class="container">
    <div class="panel panel-primary">
        <div class="panel-heading">Kategóriák</div>
        <div class="panel-body">
            <div class="row">
                <div class="col-md-6">
                    <h3>Category List</h3>
                    <ul id="tree1">

                        @foreach($categories as $category)
                            <li>
                                {{ $category->name }}

                                @if(count($category->childs))
                                    @include('categories.manageChild',['childs' => $category->childs])
                                @endif
                            </li>
                        @endforeach

                    </ul>
                </div>

                <!-- Új kategória hozzáadása -->
                <div class="col-md-6">

                    <h3>Add New Category</h3>

                    <form role="form" id="category" method="POST" action="{{ route('categories.create') }}" enctype="multipart/form-data">
                    @csrf

                    <div class="form-group {{ $errors->has('name') ? 'has-error' : '' }}">

                        <label>Name:</label>

                        <input type="text" id="name" name="name" value="" class="form-control" placeholder="Enter Title">
                        @if ($errors->has('name'))
                            <span class="text-red" role="alert">
                                <strong>{{ $errors->first('name') }}</strong>
                            </span>
                        @endif

                    </div>


                    <div class="form-group {{ $errors->has('parent_id') ? 'has-error' : '' }}">

                        <label>Category:</label>
                        <select id="parent_id" name="parent_id" class="form-control">
                            @foreach($allCategories as $rows)
                                    <option value="{{ $rows->id }}">{{ $rows->name }}</option>
                            @endforeach
                        </select>

                        @if ($errors->has('parent_id'))
                            <span class="text-red" role="alert">
                                <strong>{{ $errors->first('parent_id') }}</strong>
                            </span>
                        @endif

                    </div>

                    <div class="form-group">

                        <button type="submit" class="btn btn-success">Add New</button>

                    </div>

                    </form>
                </div>
            </div>

            <div class="row">
                <!-- ÚJ DOKUMENTUM LÉTREHOZÁSA -->
                <div class="col-md-6"> 
                    <h3>Add New Document</h3>
                    <form role="form" id="document" method="POST" action="{{ route('documents.create') }}" enctype="multipart/form-data">
                        @csrf
    
                        <div class="form-group {{ $errors->has('id') ? 'has-error' : '' }}">
    
                            <label>Name:</label>
    
                            <input type="text" id="id" name="id" value="" class="form-control" placeholder="Enter name of document">
                            @if ($errors->has('id'))
                                <span class="text-red" role="alert">
                                    <strong>{{ $errors->first('id') }}</strong>
                                </span>
                            @endif
    
                        </div>
                        
                        <div class="form-group {{ $errors->has('path') ? 'has-error' : '' }}">
    
                            <label>Name:</label>
    
                            <input type="file" id="path" name="path" value="" class="form-control" placeholder="Select the file" required>
                            @if ($errors->has('id'))
                                <span class="text-red" role="alert">
                                    <strong>{{ $errors->first('path') }}</strong>
                                </span>
                            @endif
    
                        </div>
    
                        <div class="form-group {{ $errors->has('category_id') ? 'has-error' : '' }}">
    
                            <label>Category:</label>
                            <select id="category_id" name="category_id" class="form-control">
                                @foreach($allCategories as $rows)
                                        <option value="{{ $rows->id }}">{{ $rows->name }}</option>
                                @endforeach
                            </select>
    
                            @if ($errors->has('category_id'))
                                <span class="text-red" role="alert">
                                    <strong>{{ $errors->first('category_id') }}</strong>
                                </span>
                            @endif
    
                        </div>
    
                        <div class="form-group">
                            <button type="submit" class="btn btn-success">Add New</button>
                        </div>
    
                        </form>
                </div>

                <div class="col-md-6">
                    
                    <h3>Permissions (csak alapértelmezetten Root-ra és Child_1 - re)</h3>
                    @include('categories.permission_element', [ 'category_name' => 'Root', 'permission' => $default_root_permission ])
                    @include('categories.permission_element', [ 'category_name' => 'Child_1', 'permission' => $default_child1_permission ])
                    
                </div>
            </div>
        </div>
    </div>
</div>

<hr />

@if(!is_null($selected_category) && !is_null($uploaded_documents))
<div class="container">
    <div class="display-4">{{'"' . $selected_category->name . '"' . " kategória feltöltött dokumentuma(i)"}}</div>

    <!-- Ezt jó lenne valahogy wrap - elhetővé tenni (új sorba 4 elem után), sok dokumentumnál nagyon pici kártyák lesznek -->
    <div class="container-fluid overflow-auto">

        @foreach($uploaded_documents as $document)
        
        <div class="card text-black bg-info my-3" style="max-width: 25rem;">
            <div class="card-header">
                <form action="{{ route('documents.download') }}" method="POST">
                    @csrf
                    <button class="btn btn-primary">{{ $document->id }}</button>
                    <input name="document_id" type="hidden" value="{{ $document->id }}">
                </form>
                
            </div>
            <div class="card-body">
                <h5 class="card-title"></h5>
                <p class="card-text"><small class="text-muted">{{ "verziószám: " . number_format($document->version, 1) }}</small></p>
            </div>
        </div>
        
        @endforeach

    </div>
</div>
    
@endif


<script src="{{ asset('js/treeview.js') }}"></script>

</body>

</html>
@endsection

<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Document extends Model
{
    public $fillable = ['id', 'category_id', 'path', 'version', 'time_of_upload'];

    public $incrementing = false;

    public function download() {
        return response()->download(storage_path("app\public\category_files\\" . $this->path));
    }
}

<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Document;

class DocumentsController extends Controller
{
    /**
     * Add a new category.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function create(Request $request)
    {
        $this->validate($request, [
            'id' => 'required',
            'category_id' => 'required'
        ]);
        
        $input = $request->all();

        Document::create($input);
        return back()->with('success', 'New Document added successfully.');
    }

    /**
     * Download selected file.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function download(Request $request)
    {
        $this->validate($request, [
            'document_id' => 'required',
        ]);

        $input = $request->all();
        
        if (empty($input['document_id'])) {
            return;
        }
        
        $document_id = $input['document_id'];
        $document = Document::find($document_id);
        
        if (!is_null($document)) {
            return $document->download();
        }
    }
}

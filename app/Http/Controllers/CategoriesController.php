<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Category;
use Facade\Ignition\QueryRecorder\Query;

class CategoriesController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        
    }

    /**
     * Show the hierarchical category view.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function show(Request $request)
    {   
        $user = Auth::user();
        
        $user_id = $user->id;
        $permissions = $user->permissions()->get();

        // jogosultság változtatás tesztelhetőségéhez
        $default_root_permission = $permissions->firstWhere('id', 1);
        $default_child1_permission = $permissions->firstWhere('id', 2);

        $selected_category_id = $request->query('selected_category_id');
        $selected_category = null;
        $uploaded_documents = null;

        if (!is_null($selected_category_id)) {
            $selected_category = Category::find($selected_category_id);
            $uploaded_documents = $selected_category->documents()->get();
        }

        // Kategóriák root elem
        $categories = Category::where('parent_id', '=', 0)->get();

        $allCategories = Category::all();

        return view('categories.categories',compact('categories','allCategories', 'selected_category',
         'uploaded_documents', 'user_id', 'default_root_permission', 'default_child1_permission'));
    }

    /**
     * Add a new category.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function create(Request $request)
    {
        $this->validate($request, [
            'name' => 'required',
        ]);
        
        $input = $request->all();
        
        $input['parent_id'] = empty($input['parent_id']) ? 0 : $input['parent_id'];

        Category::create($input);
        return back()->with('success', 'New Category added successfully.');
    }

    
}

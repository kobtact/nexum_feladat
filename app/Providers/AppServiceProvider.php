<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;

use App\Document;
use App\Category;
use App\Observers\DocumentObserver;
use App\Observers\CategoryObserver;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        Document::observe(DocumentObserver::class);
        Category::observe(CategoryObserver::class);
    }
}

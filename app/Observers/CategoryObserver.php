<?php

namespace App\Observers;

use App\Category;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Collection;

class CategoryObserver
{
    /**
     * Handle the document "creating" event.
     *
     * @param  \App\Category  $category
     * @return void
     */
    public function creating(Category $category)
    {
        return $this->IsRootOrParentPermitted($category->parent_id);
    }

    /**
     * Handle the category "created" event.
     *
     * @param  \App\Category  $category
     * @return void
     */
    public function created(Category $category)
    {
        //
    }

    /**
     * Handle the document "updating" event.
     *
     * @param  \App\Category  $category
     * @return void
     */
    public function updating(Category $category)
    {
        return $this->IsRootOrParentPermitted($category->parent_id);
    }

    /**
     * Handle the category "updated" event.
     *
     * @param  \App\Category  $category
     * @return void
     */
    public function updated(Category $category)
    {
        //
    }

    /**
     * Handle the category "deleted" event.
     *
     * @param  \App\Category  $category
     * @return void
     */
    public function deleted(Category $category)
    {
        //
    }

    /**
     * Handle the category "restored" event.
     *
     * @param  \App\Category  $category
     * @return void
     */
    public function restored(Category $category)
    {
        //
    }

    /**
     * Handle the category "force deleted" event.
     *
     * @param  \App\Category  $category
     * @return void
     */
    public function forceDeleted(Category $category)
    {
        //
    }

    /**
     * Van-e az aktuális User - nek jogosultsága a kategória létrehozásához
     *
     * @param Category $category
     * @return boolean
     */
    private function IsRootOrParentPermitted(?int $parent_id) {

        $user = Auth::user();

        $isCreating = false;

        // csak akkor engedjük a létrehozást, ha a szülő elem - re vagy a 
        // szülő elem gyökeréhez van hozzáférésünk
        if (!is_null($user)) {

            $categories = $user->categories()->get();

            if (!is_null($categories) && !is_null($parent_id)) {
                $isCreating = $categories->contains('id', $parent_id);
                
                // ha szülő elem nem volt benne, megnézzük, hogy annak gyökér eleme
                // benne van-e
                if (!$isCreating) {
                    $root_id = Category::find($parent_id)->root_id();
                    $isCreating = $categories->contains('id', $root_id );
                }
            }
        }

        error_log($isCreating ? 'true' : 'false');

        return $isCreating;
    }
}

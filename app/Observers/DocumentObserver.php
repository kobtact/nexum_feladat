<?php

namespace App\Observers;

use App\Document;
use Carbon\Carbon;
use Illuminate\Support\Facades\Storage;
use Illuminate\Http\File;

class DocumentObserver
{
    /**
     * Handle the document "creating" event.
     *
     * @param  \App\Document  $document
     * @return void
     */
    public function creating(Document $document)
    {
        $isCreating = true;

        // aktuális date time
        $document->time_of_upload = Carbon::now();

        // eltároljuk a fájlt
        $storage_path = Storage::putFile('\public\category_files\\', new File($document->path));

        // eltárolás után csak a fájlt nevet elpakoljuk
        $document->path = basename($storage_path);

        $existing_document = Document::find($document->id);


        if(!is_null($existing_document)) {
            $isCreating = false;

            $existing_document->path = $document->path;
            $existing_document->category_id = $document->category_id;
            $existing_document->time_of_upload = $document->category_id;
            $existing_document->save();
        }

        return $isCreating;
    }

    /**
     * Handle the document "created" event.
     *
     * @param  \App\Document  $document
     * @return void
     */
    public function created(Document $document)
    {
        //
    }

    /**
     * Handle the document "updating" event.
     *
     * @param  \App\Document  $document
     * @return void
     */
    public function updating(Document $document)
    {
        // aktuális date time
        $document->version = $document->version + 1;
    }
    
    /**
     * Handle the document "updated" event.
     *
     * @param  \App\Document  $document
     * @return void
     */
    public function updated(Document $document)
    {
        //
    }

    /**
     * Handle the document "deleted" event.
     *
     * @param  \App\Document  $document
     * @return void
     */
    public function deleted(Document $document)
    {
        //
    }

    /**
     * Handle the document "restored" event.
     *
     * @param  \App\Document  $document
     * @return void
     */
    public function restored(Document $document)
    {
        //
    }

    /**
     * Handle the document "force deleted" event.
     *
     * @param  \App\Document  $document
     * @return void
     */
    public function forceDeleted(Document $document)
    {
        //
    }
}

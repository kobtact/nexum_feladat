<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Permission extends Model
{
    public $fillable = ['user_id', 'category_id', 'upload', 'download'];

    public function user() {
        return $this->belongsTo(User::class, 'id', 'user_id');
    }
}

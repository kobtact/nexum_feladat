<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Category extends Model
{
    public $fillable = ['name', 'parent_id'];

    /**
     * visszaadja az adott node gyökér elemének ID-jét
     *
     * @return void
     */
    public function root_id() {
        if ($this->parent_id == 0) {
            return $this->id;
        }
        else {
            return $this->parent()->get()->first()->root_id();
        }
    }

    public function parent() {
        return $this->hasOne(Category::class, 'id', 'parent_id');
    }

    // a tábla saját magára mutat
    public function childs() {
        return $this->hasMany(Category::class, 'parent_id', 'id');
    }

    public function documents() {
        return $this->hasMany(Document::class, 'category_id', 'id');
    }
}

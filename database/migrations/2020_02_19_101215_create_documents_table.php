<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateDocumentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('documents', function (Blueprint $table) {
            $table->string('id');
            $table->primary('id'); // elsődleges kulcsként használom, hogy a verzió kezelés egyszerőbb legyen

            $table->string('path'); // local store - ban a fájl elérési útvonala
            
            $table->unsignedBigInteger('category_id');
            
            $table->decimal('version', 3, 1)->default('1.0')->nullable(false);
            $table->dateTime('time_of_upload')->default()->nullable(false);

            $table->foreign('category_id')->references('id')->on('categories')
            ->onDelete('cascade')
            ->OnUpdate('cascade');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('documents');
    }
}

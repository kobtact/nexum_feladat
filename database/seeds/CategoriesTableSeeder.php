<?php

use Illuminate\Database\Seeder;

class CategoriesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Eloquent::unguard();

        DB::table('categories')->delete();
        DB::table('categories')->insert([
            "name" => "Root",
        "parent_id" => 0
        ]);
        DB::table('categories')->insert([
            "name" => "Child_1",
            "parent_id" => 1
        ]);
    }
}

<?php

use Illuminate\Database\Seeder;

class PermissionsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('permissions')->delete();
        DB::table('permissions')->insert([
            "user_id" => "1",
            "category_id" => "1",
            "upload" => "1",
            "download" => "1"
        ]);
        DB::table('permissions')->insert([
            "user_id" => "1",
            "category_id" => "2",
            "upload" => "0",
            "download" => "0"
        ]);
    }
}

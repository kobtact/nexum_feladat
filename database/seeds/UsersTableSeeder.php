<?php

use Illuminate\Database\Seeder;
use App\User;

class UsersTableSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->delete();
        DB::table('users')->insert([
            "name" => "Adele Minotaurus",
            "email" => "admin@admin.com",
            "email_verified_at" => null,
            "password" => Hash::make("admin"),
            "username" => "admin",
        ]);
    }
}

<?php

use Illuminate\Database\Seeder;
use Carbon\Carbon;

class DocumentsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Eloquent::unguard();

        DB::table('documents')->delete();
        DB::table('documents')->insert([
            "id" => "test_document",
            "path" => "brúszli.jpg",
            "category_id" => "2",
            "version" => "1.0",
            "time_of_upload" => Carbon::now(),
        ]);
    }
}
